################################################################################
#                                                                              #
#                   Représentation de réseau avec igraph                       #
#                             Hugues Pecout                                    #
#                               5/10/2023                                      #
#                                                                              #
################################################################################

################################################################################
#                                                                              # 
# CONSIGNES : REMPLACER les "..." par une valeur (objet, valeur d'argument)    #
#                                                                              #
################################################################################


##########------------------ CHARGEMENT PACKAGES ---------------------##########
library(igraph)
library(sf)


################################################################################
##########---------------------- IMPORT DONNEES ----------------------##########

#--------------------------- DATA RELAIS POSTE --------------------------------#

### Données brutes prétraitées : Prepa_data_relais_poste.R

# Import des points relais
relais <- read.csv(file = "data/TD_igraph/RELAIS_1833.csv")

# Import des routes
routes  <- read.csv(file = "data/TD_igraph/ROUTES_1833.csv")


## Affichage des données
# View(routes)
# View(relais)



##########----------------- CONSTRUCTION DU RESEAU -------------------##########                            

# Construction objet igraph à partir d'une table de lien et d'un tablaeu de sommets
mon_reseau <- graph_from_data_frame(d = routes,
                                    vertices = relais, 
                                    directed = FALSE)

# Ajout d'un nom
graph_attr(mon_reseau, "name") <- "Relais des postes"

# Ajout d'auteur.es
graph_attr(mon_reseau, "author") <- "Nicolas Verdier, Anne Bretagnolle (2007)"

# Ajout d'une citation
graph_attr(mon_reseau, "citation") <- "Nicolas Verdier, Anne Bretagnolle. L'extension du réseau des routes de poste en France, de 1708 à 1833. \nHistoire des réseaux postaux en Europe du XVIIIe au XXIe siècle, May 2007, Paris, France. pp.155-193. ⟨halshs-00144693⟩"


# Affichage du réseau
plot(mon_reseau)



##########-------- AMELIORATION DE LA REPRESENTATION GRAPHIQUE -------##########

### Test d'une "layout" prédéfinie
ma_layout <- layout_nicely(mon_reseau)

# Affichage
plot(mon_reseau, 
     vertex.label = NA, 
     layout = ma_layout)


# Modification taille de sommet et couleur
plot(mon_reseau, 
     vertex.color = "#0060AE",                # couleur des sommets
     vertex.frame.color = "#0060AE",          # couleur des contours des sommets
     vertex.size = 1,                         # taille des sommets
     vertex.label = NA,                       # étiquettes des sommets
     edge.color = "#FFE413",                  # couleur des arêtes
     edge.width = 0.6,                        # largeur des arêtes
     layout = ma_layout,                   # Mise en page (matrice de coordonnées)
     margin =  c(0.2,0,0,0),                  # marges
     main = graph_attr(mon_reseau, "name"))   # Titre

# Ajout de la source
mtext(graph_attr(mon_reseau, "citation"), side=1, cex = 0.6, line=-0.5)




##########-------- REPRESENTATION GRAPHIQUE GEOREFERENCEE ------------##########


#------------------------------ FOND DE CARTE ---------------------------------#

### Données IGN (BD ADMIN-EXPRESS)

# Départements FR
dep_fr <- st_read(dsn = "data/basemap/basemap_fr.gpkg", layer = "dep_fr")

# Affichage des données
plot(st_geometry(dep_fr))



#------------------------ LAYOUT GEOGRAPHIQUE ---------------------------------#

coordinates <- cbind(long = V(mon_reseau)$COORDXLAMB, lat = V(mon_reseau)$COORDYLAMB)

# Matrice des cordonnées :
head(coordinates)

# Gestion des marges de la fenêtre graphique
par(mar=c(0,0,2,0))

# Affichage du fond de carte
plot(st_geometry(dep_fr), 
     col = "grey75",
     border = "white", 
     lwd = 0.2)

# Affichage du graphe
plot(mon_reseau, 
     vertex.label = NA, 
     layout = coordinates, 
     rescale = FALSE,
     add = TRUE)



##########------------ REPRESENTATION D'INDICATEURS ------------------##########

# 1) Représenter l'ensemble du relais des postes de 1833

## Représenter la pente (Avg_Slope) des routes en faisant l'épaisseur des liens
E(mon_reseau)$width  <- E(mon_reseau)$duree / 100

## Représenté la Centralité de degré (degree()) des poste en faisant varier la taille des sommet
V(mon_reseau)$size <- degree(mon_reseau) * 170000

# Affichage du fond de carte des départements
plot(st_geometry(dep_fr), 
     col = "grey75",
     border = "white", 
     lwd = 0.2)

# Affichage du réseau des relais poste
plot(mon_reseau, 
     vertex.color= "black",
     vertex.frame.color = NA,
     edge.color = "lightblue4",
     layout = coordinates,
     vertex.label = NA,
     vertex.frame.color = NA,
     rescale = FALSE,
     add = TRUE)


# 2) Représenter le réseau existant en 1833 en indiquant les relais et le routes qui existaient déjà en 1632

# Différencier les relais qui existaient déjà (ou pas) avec de la couleur
V(mon_reseau)$color <-  ifelse(V(mon_reseau)$RELAIS1632 %in% "t", "black", "red3") 

# Différencier les routes postales qui existaient déjà (ou pas) avec de la couleur
E(mon_reseau)$color <-  ifelse(E(mon_reseau)$routes1833 %in% "t", "black", "red3")  



# Affichage du fond de carte des départements
plot(st_geometry(dep_fr), 
     col = "grey75",
     border = "white", 
     lwd = 0.2,
     main = "Réseau des relais poste en 1833")

# Affichage du réseau des relais poste
plot(mon_reseau, 
     vertex.frame.color = NA,
     layout = coordinates,
     vertex.label = NA,
     vertex.frame.color = NA,
     rescale = FALSE,
     add = TRUE)


# Ajout de la source
mtext(graph_attr(mon_reseau, "citation"), side=1, cex = 0.6, line=-2)



##########---------------- IGRAPH et les légendes --------------------##########

# locator(1)

# Legende couleur sommets
legend(x = 8749.791, y = 7117027,
       legend = c("Depuis 1632", "Après 1632"),
       pch=20,
       col= unique(V(mon_reseau)$color),
       pt.cex=3,
       bty="o",
       title = "Ancienneté du réseau")


